﻿using NLog;

using Phoenix.Framework.Extensions;
using Phoenix.Framework.Network.Messaging;
using Phoenix.Framework.Security.Cryptography;

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Phoenix.Framework.Security.DiffieHellman
{
    internal abstract class KeyExchangeBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private protected readonly Random _random;
        protected readonly Blowfish _blowfish;

        private protected uint _prime;
        private protected uint _generator;
        private protected uint _private;
        private protected uint _localPublic;
        private protected uint _remotePublic;
        private protected uint _commonSecret;

        private protected readonly byte[] _key;

        private readonly byte[] _blowfishKey;
        private protected byte[] _localChallenge;
        private protected byte[] _remoteChallenge;

        private bool _hasLocalSetup;
        private bool _hasRemoteSetup;
        private protected bool _hasCompleted;

        public void GetSecureKey(Span<byte> key)
        {
            if (!_hasCompleted)
                throw new KeyExchangeException("Key exchange has not yet been completed.");

            _key.CopyTo(key);
            KeyExchangeHelper.KeyTransformValue(key, _commonSecret, 0x03);

            if (Logger.IsTraceEnabled)
                Logger.Trace($"SecureKey: {BitConverter.ToString(key.ToArray())}");
        }

        public KeyExchangeBase()
        {
            _key = new byte[sizeof(ulong)];

            _blowfishKey = new byte[sizeof(ulong)];
            _localChallenge = new byte[sizeof(ulong)];
            _remoteChallenge = new byte[sizeof(ulong)];

            _random = new Random();
            _blowfish = new Blowfish();

            _private = _random.NextUInt() & int.MaxValue;
            if (Logger.IsTraceEnabled)
                Logger.Trace($"Private: {_private}");
        }

        private protected void SetupLocal(uint generator, uint prime)
        {
            if (_hasLocalSetup)
                throw new KeyExchangeException("Local setup has already been completed.");

            _generator = generator;
            _prime = prime;
            _localPublic = KeyExchangeHelper.G_pow_X_mod_P(_generator, _private, _prime);
            _hasLocalSetup = true;

            if (Logger.IsTraceEnabled)
            {
                Logger.Trace($"Generator: {_generator}");
                Logger.Trace($"Prime: {_prime}");
                Logger.Trace($"LocalPublic: {_localPublic}");
            }
        }

        private protected void SetupRemote(uint remotePublic)
        {
            if (_hasRemoteSetup)
                throw new KeyExchangeException("Remote setup has already been completed.");

            if (!_hasLocalSetup)
                throw new KeyExchangeException("Local setup has not yet been completed.");

            _remotePublic = remotePublic;

            _commonSecret = KeyExchangeHelper.G_pow_X_mod_P(_remotePublic, _private, _prime);

            Span<byte> key = stackalloc byte[sizeof(ulong)];
            if (this is KeySender)
            {
                this.CalculateKey(key, ref _localPublic, ref _remotePublic);
                _blowfish.Initialize(key);
                this.CalculateChallenge(_localChallenge, ref _remotePublic, ref _localPublic);
            }
            else if (this is KeyRecipent)
            {
                this.CalculateKey(key, ref _remotePublic, ref _localPublic);
                _blowfish.Initialize(key);
                this.CalculateChallenge(_localChallenge, ref _localPublic, ref _remotePublic);
            }

            _hasRemoteSetup = true;
            
            if (Logger.IsTraceEnabled)
            {
                Logger.Trace($"RemotePublic: {_remotePublic}");
                Logger.Trace($"CommonSecret: {_commonSecret}");
                Logger.Trace($"BlowfishKey: {BitConverter.ToString(_blowfishKey)}");
                Logger.Trace($"LocalChallange: {BitConverter.ToString(_localChallenge)}");
            }

        }

        public abstract void WriteSetup(Message msg);

        internal abstract void ReadSetup(Message msg);

        internal virtual void WriteChallenge(Message msgAck)
        {
            if (this is KeySender)
            {
                Span<byte> key = stackalloc byte[sizeof(ulong)];
                this.CalculateKey(key, ref _localPublic, ref _remotePublic);
                _blowfish.Initialize(key);

                this.CalculateChallenge(_localChallenge, ref _localPublic, ref _remotePublic);
            }

            _blowfish.Encode(_localChallenge, _localChallenge);
            msgAck.TryWrite<byte>(_localChallenge);
        }

        internal void CalculateChallenge(Span<byte> key, ref uint secret1, ref uint secret2)
        {
            MemoryMarshal.Write(key.Slice(0), ref secret1);
            MemoryMarshal.Write(key.Slice(4), ref secret2);
            KeyExchangeHelper.KeyTransformValue(key, _commonSecret, (byte)(secret1 & 7));
        }

        internal void CalculateKey(Span<byte> key, ref uint secret1, ref uint secret2)
        {
            MemoryMarshal.Write(key.Slice(0), ref secret1);
            MemoryMarshal.Write(key.Slice(4), ref secret2);
            KeyExchangeHelper.KeyTransformValue(key, _commonSecret, (byte)(_commonSecret & 3));
        }
               
        internal abstract void ReadChallenge(Message msg);
    }
}