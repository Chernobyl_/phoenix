﻿using System;

namespace Phoenix.Framework.Security.DiffieHellman
{
    internal static class KeyExchangeHelper
    {
        public static uint G_pow_X_mod_P(uint G, uint X, uint P)
        {
            long result = 1;
            long mult = G;
            if (X == 0)
                return 1;

            while (X != 0)
            {
                if ((X & 1) > 0)
                    result = (mult * result) % P;

                X >>= 1;
                mult = (mult * mult) % P;
            }
            return (uint)result;
        }

        public static void KeyTransformValue(Span<byte> value, uint key, byte keyByte)
        {
            value[0] ^= (byte)(value[0] + ((key >> 00) & byte.MaxValue) + keyByte);
            value[1] ^= (byte)(value[1] + ((key >> 08) & byte.MaxValue) + keyByte);
            value[2] ^= (byte)(value[2] + ((key >> 16) & byte.MaxValue) + keyByte);
            value[3] ^= (byte)(value[3] + ((key >> 24) & byte.MaxValue) + keyByte);

            value[4] ^= (byte)(value[4] + ((key >> 00) & byte.MaxValue) + keyByte);
            value[5] ^= (byte)(value[5] + ((key >> 08) & byte.MaxValue) + keyByte);
            value[6] ^= (byte)(value[6] + ((key >> 16) & byte.MaxValue) + keyByte);
            value[7] ^= (byte)(value[7] + ((key >> 24) & byte.MaxValue) + keyByte);
        }
    }
}