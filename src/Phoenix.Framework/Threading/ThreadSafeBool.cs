﻿using System.Threading;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// Thread-safe boolean implementation with backed value using interlocked CompareExchange.
    /// See https://stackoverflow.com/a/49233660
    /// </summary>
    public class ThreadSafeBool
    {
        private int _threadSafeBackValue = 0;

        /// <summary>
        /// Initializes a new instance of <see cref="ThreadSafeBool"/>.
        /// </summary>
        /// <param name="initialValue">The initial value.</param>
        public ThreadSafeBool(bool initialValue = false) =>
            this.Value = initialValue;

        /// <summary>
        /// Gets or sets the backed thread-safe value.
        /// </summary>
        public bool Value
        {
            get => Interlocked.CompareExchange(ref _threadSafeBackValue, 1, 1) == 1;
            set
            {
                if (value == true)
                    Interlocked.CompareExchange(ref _threadSafeBackValue, 1, 0);
                else Interlocked.CompareExchange(ref _threadSafeBackValue, 0, 1);
            }
        }
    }
}
