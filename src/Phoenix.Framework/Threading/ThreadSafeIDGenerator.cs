﻿using System.Threading;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// Network session id generator using interlocked.
    /// </summary>
    public class ThreadSafeIDGenerator
    {
        private const int DefaultID = 0;

        private int _lastSessionId = DefaultID;

        /// <summary>
        /// Gets the next session id.
        /// </summary>
        /// <returns>The session id.</returns>
        public int Generate()
        {
            return Interlocked.Increment(ref _lastSessionId) & 0x7FFFFFFF;
        }

        /// <summary>
        /// Resets the generator state.
        /// </summary>
        public void Reset() => Interlocked.Exchange(ref _lastSessionId, DefaultID);
    }
}