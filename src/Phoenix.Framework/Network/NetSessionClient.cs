﻿using Phoenix.Framework.Network.Messaging.Protocol;

namespace Phoenix.Framework.Network
{
    public class NetSessionClient : NetSession
    {
        public NetSessionClient(NetEngine engine, int id) : base(engine, id)
        {
            this.Protocol = new ClientMessageProtocol(this);
        }
    }
}