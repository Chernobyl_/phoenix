﻿namespace Phoenix.Framework.Network.Config
{
    public interface INetClientConfig : INetConfig
    {
        bool KeepAlive { get; }
        int ConnectionTimeout { get; }
    }
}