﻿using Phoenix.Framework.Memory;
using System.Net.Sockets;

namespace Phoenix.Framework.Network.EventArgs
{
    internal interface INetEventArgsPool<TEventArgs> : IObjectPool<TEventArgs>
        where TEventArgs : SocketAsyncEventArgs
    {
    }
}