﻿using Phoenix.Framework.Memory;

using System;
using System.Net.Sockets;

namespace Phoenix.Framework.Network.EventArgs
{
    //public delegate void NetSocketEventHandler(object sender, SocketAsyncEventArgs e);

    internal class NetEventArgsPool<TEventArgs> : ObjectPool<TEventArgs>, INetEventArgsPool<TEventArgs>
        where TEventArgs : SocketAsyncEventArgs, new()
    {
        private protected readonly EventHandler<SocketAsyncEventArgs> _handler;

        public NetEventArgsPool(int size, EventHandler<SocketAsyncEventArgs> handler) : base(size, false)
        {
            _handler = handler;
        }

        protected override TEventArgs Create()
        {
            var obj = new TEventArgs();
            obj.Completed += _handler;
            return obj;
        }

        protected override void Clean(in TEventArgs obj)
        {
            obj.RemoteEndPoint = null;
            obj.AcceptSocket = null;
            obj.UserToken = null;

            obj.SetBuffer(null, 0, 0);
        }

        protected override void Destroy(in TEventArgs obj)
        {
            obj.SetBuffer(null, 0, 0);
            obj.Completed -= _handler;
            obj.Dispose();
        }
    }
}