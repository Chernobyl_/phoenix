﻿using System.Net.Sockets;
using System.Threading;

namespace Phoenix.Framework.Network.EventArgs
{
    internal class ConnectNetEventArgs : NetEventArgs
    {
        private CancellationTokenSource _cts;

        public void CancelAfter(int timeout = Timeout.Infinite)
        {
            _cts = new CancellationTokenSource();
            _cts.Token.Register(() => Socket.CancelConnectAsync(this));
            _cts.CancelAfter(timeout);
        }

        protected override void OnCompleted(SocketAsyncEventArgs e)
        {
            _cts.Dispose();
            base.OnCompleted(e);
        }
    }
}