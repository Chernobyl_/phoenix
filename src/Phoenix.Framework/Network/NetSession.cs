﻿using Phoenix.Framework.Network.Messaging;
using Phoenix.Framework.Network.Messaging.Protocol;

using System.Net.Sockets;

namespace Phoenix.Framework.Network
{
    public delegate void NetEventHandler(NetSession session);

    public abstract class NetSession
    {
        public int ID { get; set; }
        public NetTrafficMonitor TrafficMonitor { get; private set; }
        public NetKeepAliveMonitor KeepAliveMonitor { get; private set; }

        internal NetEngine Engine { get; }
        internal Socket Socket { get; set; }
        internal MessageProtocol Protocol { get; set; }
        internal MessageDecoder Decoder => this.Protocol.Decoder;
        internal MessageEncoder Encoder => this.Protocol.Encoder;

        public byte[] ReceiveBuffer { get; internal set; }

        public event NetEventHandler Ready;

        public NetSession(NetEngine engine, int id)
        {
            this.Engine = engine;
            this.ID = id;

            this.ReceiveBuffer = new byte[NetHelper.BUFFER_SIZE];

            this.TrafficMonitor = new NetTrafficMonitor();
            this.KeepAliveMonitor = new NetKeepAliveMonitor();
        }

        public virtual void Initialize()
        {
            this.Protocol.Initialize();
            this.Protocol.Completed += this.Protocol_Completed;
        }

        private void Protocol_Completed()
        {
            this.Ready?.Invoke(this);
        }

        public void Disconnect(NetDisconnectReason reason)
        {
            this.Engine.Disconnect(this, reason);
        }

        public void Send(Message message)
        {
            this.Engine.Send(this, message);
        }
    }
}