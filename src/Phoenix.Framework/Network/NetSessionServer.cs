﻿using Phoenix.Framework.Network.Messaging;
using Phoenix.Framework.Network.Messaging.Protocol;

namespace Phoenix.Framework.Network
{
    public class NetServerSession : NetSession
    {
        public NetServerSession(NetEngine engine, int id) : base(engine, id)
        {
            const MessageEncodeOption options = MessageEncodeOption.Encryption | MessageEncodeOption.EDC | MessageEncodeOption.KeyExchange;
            this.Protocol = new ServerMessageProtocol(this, options);
        }
    }
}