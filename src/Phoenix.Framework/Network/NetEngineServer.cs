﻿using NLog;

using Phoenix.Framework.Network.Config;
using Phoenix.Framework.Network.EventArgs;

using System;
using System.Net.Sockets;

namespace Phoenix.Framework.Network
{
    public abstract class NetEngineServer : NetEngine
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Socket _listener = NetHelper.CreateTcpSocket();

        private readonly int _backlog;
        private readonly INetEventArgsPool<SocketAsyncEventArgs> _acceptEventArgsPool;

        public NetEngineServer(INetServerConfig config) : base(config)
        {
            _backlog = config.Backlog;

            _acceptEventArgsPool = new NetEventArgsPool<SocketAsyncEventArgs>(config.SocketCount, this.AcceptCompleted);
            _acceptEventArgsPool.Initialize();
        }

        public void Start(string hostOrIP, ushort port)
        {
            _listener.Bind(NetHelper.ToEndPoint(hostOrIP, port));
            _listener.Listen(_backlog);
            Logger.Info($"Listening on {hostOrIP}:{port}");

            this.Accept();
        }

        #region Accept

        private void Accept()
        {
            if (!_acceptEventArgsPool.TryTake(out SocketAsyncEventArgs acceptArgs))
            {
                Logger.Error($"{nameof(Accept)}: Failed to take {nameof(SocketAsyncEventArgs)}!");
                return;
            }

            if (!_socketPool.TryTake(out Socket socket))
            {
                Logger.Error($"{nameof(Accept)}: Failed to take {nameof(Socket)}!");

                _acceptEventArgsPool.TryReturn(acceptArgs);
                return;
            }

            try
            {
                acceptArgs.AcceptSocket = socket;

                // If the I/O operation is pending, the SocketAsyncEventArgs.Completed event will be raised upon completion of the operation.
                if (_listener.AcceptAsync(acceptArgs))
                    return;

                // The I/O operation completed synchronously, SocketAsyncEventArgs.Completed event will not be raised.
                this.AcceptCompleted(socket, acceptArgs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                _socketPool.TryReturn(socket);
                _acceptEventArgsPool.TryReturn(acceptArgs);
            }
        }

        private void AcceptCompleted(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                if (e.SocketError != SocketError.Success)
                {
                    Logger.Debug($"{nameof(AcceptCompleted)}: {e.SocketError}");
                    return;
                }

                var session = this.SessionManager.CreateSession();
                session.Socket = e.AcceptSocket;

                this.OnAccepted(session);
                this.Receive(session);
            }
            finally
            {
                _acceptEventArgsPool.TryReturn(e);
                this.Accept();
            }
        }

        protected virtual void OnAccepted(NetSession session)
        {
            session.Initialize();
        }

        #endregion Accept
    }
}