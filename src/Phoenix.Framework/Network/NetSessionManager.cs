﻿using NLog;

using Phoenix.Framework.Threading;

using System.Collections.Concurrent;

namespace Phoenix.Framework.Network
{
    public class NetSessionManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly NetEngine _engine;
        private readonly INetSessionFactory _factory;

        private readonly ThreadSafeIDGenerator _generator;
        private readonly ConcurrentDictionary<int, NetSession> _sessions;

        public NetSessionManager(NetEngine engine, INetSessionFactory factory)
        {
            _engine = engine;
            _factory = factory;

            _generator = new ThreadSafeIDGenerator();
            _sessions = new ConcurrentDictionary<int, NetSession>();
        }

        internal NetSession CreateSession()
        {
            var id = _generator.Generate();
            return _sessions[id] = _factory.Create(_engine, id);
        }

        internal void DestroySession(NetSession session) => this.DestroySession(session.ID);

        internal void DestroySession(int id)
        {
            if (!_sessions.TryRemove(id, out NetSession session))
            {
                Logger.Error($"Failed to destroy session #{id}");
                return;
            }
        }

        public bool TryFindSessionById(int id, out NetSession session) => _sessions.TryGetValue(id, out session);
    }
}