﻿using System;

namespace Phoenix.Framework.Network
{
    public class NetKeepAliveMonitor
    {
        private DateTime _lastReceiveTime;

        public bool IsAlive
        {
            get
            {
                const int MAX_TIMEOUT_MS = 15000; // TODO: From config?
                if (_lastReceiveTime == default || DateTime.UtcNow.Subtract(_lastReceiveTime).TotalMilliseconds > MAX_TIMEOUT_MS)
                    return false;

                return true;
            }
        }

        public void OnReceive()
        {
            _lastReceiveTime = DateTime.UtcNow;
        }
    }
}