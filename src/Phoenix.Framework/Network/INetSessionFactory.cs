﻿namespace Phoenix.Framework.Network
{
    public interface INetSessionFactory
    {
        NetSession Create(NetEngine engine, int id);
    }
}