﻿using System;

namespace Phoenix.Framework.Network.Messaging
{
    public interface IMessage : IDisposable, ICloneable
    {
        MessageID ID { get; set; }

        Message Clone();
    }
}