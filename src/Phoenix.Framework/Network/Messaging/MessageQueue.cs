﻿using NLog;

using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network.Messaging
{
    internal class MessageQueue
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MessageManager _manager;
        private readonly MessageProfiler _profiler;
        private readonly CancellationToken _cancellationToken;
        private readonly BlockingCollection<MessageQueueJob> _collection;
        private readonly Task _task;

        public MessageQueue(MessageManager manager, MessageProfiler profiler, CancellationToken cancellationToken)
        {
            _manager = manager;
            _profiler = profiler;
            _cancellationToken = cancellationToken;
            _collection = new BlockingCollection<MessageQueueJob>();

            _task = Task.Factory.StartNew(this.Consume, _cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        internal void Process(NetSession session, Message message)
        {
            var job = new MessageQueueJob(session, message); // TODO: Pool?
            _collection.TryAdd(job);
        }

        private void Consume()
        {
            var watch = new Stopwatch();
            while (true)
            {
                if (_cancellationToken.IsCancellationRequested)
                    return;

                if (!_collection.TryTake(out MessageQueueJob job, Timeout.Infinite, _cancellationToken))
                    return;

                try
                {
                    watch.Restart();

                    // TODO: Timeout for handling.
                    //var cts = new CancellationTokenSource();
                    //var handleTask = _manager.Handle(item.Session, item.Message, cts.Token);
                    //var timeoutTask = Task.Delay(10000);
                    //await Task.WhenAny(handleTask, timeoutTask);
                    if (_manager.Handle(job.Session, job.Message) != MessageResult.Success)
                        job.Session.Disconnect(NetDisconnectReason.NetEngine);

                    watch.Stop();
                    _profiler.OnHandle(job.Message, watch.ElapsedTicks);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, $"Message={job.Message}; Session={job.Session}");
                    job.Session.Disconnect(NetDisconnectReason.NetEngine);
                }
                finally
                {
                    job.Message.Dispose();
                }
            }
        }
    }
}