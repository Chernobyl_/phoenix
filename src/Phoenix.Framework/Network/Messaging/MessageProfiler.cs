﻿using System.Collections.Concurrent;

namespace Phoenix.Framework.Network.Messaging
{
    public class MessageProfiler
    {
        private readonly ConcurrentDictionary<MessageID, MessageTrafficMonitor> _messageTrafficInfos;

        public MessageProfiler()
        {
            _messageTrafficInfos = new ConcurrentDictionary<MessageID, MessageTrafficMonitor>();
        }

        public void OnReceive(Message msg)
        {
            if (!_messageTrafficInfos.TryGetValue(msg.ID, out MessageTrafficMonitor info))
                _messageTrafficInfos[msg.ID] = info = new MessageTrafficMonitor();

            info.OnReceive(msg.Size);
        }

        public void OnSend(Message msg)
        {
            if (!_messageTrafficInfos.TryGetValue(msg.ID, out MessageTrafficMonitor info))
                _messageTrafficInfos[msg.ID] = info = new MessageTrafficMonitor();

            info.OnSend(msg.Size);
        }

        internal void OnHandle(Message msg, long handlerTicks)
        {
            if (!_messageTrafficInfos.TryGetValue(msg.ID, out MessageTrafficMonitor info))
                _messageTrafficInfos[msg.ID] = info = new MessageTrafficMonitor();

            info.Handled(handlerTicks);
        }
    }
}