﻿namespace Phoenix.Framework.Network.Messaging
{
    /*
        FrameworkData = 0x1,
        FrameworkNoDir = 0x2,
        GameNoDir = 0x3,
        ShardReq = 0x4,
        HandshakeReq = 0x5,
        FrameworkReq = 0x6,
        GameReq = 0x7,
        ShardAck = 0x8,
        HandshakeAck = 0x9,
        FrameworkAck = 0xA,
        GameAck = 0xB,
        ReservedC = 0xC,
        ReservedD = 0xD,
        ReservedE = 0xE,
        ReservedF = 0xF,
     */
    public enum MessageType : byte
    {
        None = 0,
        NetEngine = 1,
        Framework = 2,
        Game = 3,
    }
}