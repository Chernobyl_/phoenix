﻿using Phoenix.Framework.Network.Messaging.Protocol;

namespace Phoenix.Framework.Network.Messaging
{
    internal class MessageEncoder
    {
        private readonly MessageProtocol _protocol;

        public MessageEncoder(MessageProtocol protocol)
        {
            _protocol = protocol;
        }

        public bool TryEncode(Message msg) => _protocol.TryEncode(msg);
    }
}