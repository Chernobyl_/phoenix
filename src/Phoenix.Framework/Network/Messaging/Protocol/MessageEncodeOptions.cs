﻿using System;

namespace Phoenix.Framework.Network.Messaging
{
    [Flags]
    public enum MessageEncodeOption : byte
    {
        None = 0,

        Disabled = 1,

        /// <summary>
        /// Blowfish
        /// </summary>
        Encryption = 2,

        /// <summary>
        /// Error detection code with Sequence and Cyclic redundancy check
        /// </summary>
        EDC = 4,

        KeyExchange = 8,

        KeyChallenge = 16,
    }
}