﻿namespace Phoenix.Framework.Network.Messaging.Protocol
{
    public enum MessageProtocolState : byte
    {
        None,
        WaitSetup,
        WaitChallenge,
        WaitAccept,
        Completed,
    }
}