﻿using Phoenix.Framework.Network.Messaging.Protocol;

using System;

namespace Phoenix.Framework.Network.Messaging
{
    internal class MessageDecoder
    {
        private readonly MessageProtocol _protocol;

        public MessageDecoder(MessageProtocol protocol)
        {
            _protocol = protocol;
        }

        internal ProtocolDecodeResult TryDecode(Span<byte> segment, int bytesTransferred) => _protocol.TryDecode(segment, bytesTransferred);

        internal bool TryTake(out Message msg) => _protocol.TryTakeDecoded(out msg);
    }
}