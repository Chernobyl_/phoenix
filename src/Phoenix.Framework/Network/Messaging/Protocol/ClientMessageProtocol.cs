﻿using Phoenix.Framework.Security.DiffieHellman;

namespace Phoenix.Framework.Network.Messaging.Protocol
{
    internal class ClientMessageProtocol : MessageProtocol
    {
        public ClientMessageProtocol(NetSession session) : base(session)
        {
            this.KeyExchange = new KeyRecipent();
        }

        internal override void Initialize()
        {
            base.Initialize();

            this.State = MessageProtocolState.WaitSetup;
        }

        internal override ProtocolDecodeResult ValidateSignature(Message msg)
        {
            if (msg.Sequence != default)
                return ProtocolDecodeResult.InvalidSequence;

            if (msg.CRC != default)
                return ProtocolDecodeResult.InvalidCRC;

            return ProtocolDecodeResult.Success;
        }

        internal override void Sign(Message msg)
        {
            msg.Sequence = this.Sequence.Next();

            msg.CRC = 0;
            msg.CRC = this.CRC.Compute(msg.GetBufferAt(0, msg.Size + Message.HeaderSize));
        }

        internal override MessageResult HandleReq(Message msg)
        {
            if (!msg.TryRead(out MessageEncodeOption option))
                return MessageResult.Error;

            // Set protocol option if we have none
            if (_option == MessageEncodeOption.None)
                _option = option;

            // Setup Blowfish
            if ((option & MessageEncodeOption.Encryption) != 0)
            {
                if (!msg.TryRead<byte>(_blowfishKey))
                    return MessageResult.Error;

                this.Blowfish.Initialize(_blowfishKey);
            }

            // Setup EDC
            if ((option & MessageEncodeOption.EDC) != 0)
            {
                if (!msg.TryRead(out uint sequenceSeed))
                    return MessageResult.Error;

                if (!msg.TryRead(out uint crcSeed))
                    return MessageResult.Error;

                this.Sequence = new MessageSequence(sequenceSeed);
                this.CRC = new MessageCRC(crcSeed);
            }

            // Handshake
            if ((option & MessageEncodeOption.KeyExchange) != 0)
                return this.HandshakeSetup(msg);

            // Challenge
            if ((option & MessageEncodeOption.KeyChallenge) != 0)
                return this.HandshakeChallenge(msg);

            // Make sure we're in the correct state to accept handshake-less connection.
            if (this.State != MessageProtocolState.None)
                return MessageResult.Error;

            using (var msgAccept = MessagePool.Rent())
            {
                msgAccept.ID = AckID;

                _session.Send(msgAccept);
            }
            this.State = MessageProtocolState.Completed;

            return MessageResult.Success;
        }

        internal override MessageResult HandleAck(Message msg)
        {
            return MessageResult.Error;
        }

        private MessageResult HandshakeChallenge(Message msg)
        {
            if (this.State != MessageProtocolState.WaitChallenge)
                return MessageResult.Error;

            this.KeyExchange.ReadChallenge(msg);

            using (var msgAccept = MessagePool.Rent())
            {
                msgAccept.ID = AckID;

                _session.Send(msgAccept);
            }

            this.KeyExchange.GetSecureKey(_blowfishKey);
            this.Blowfish.Initialize(_blowfishKey);

            this.State = MessageProtocolState.Completed;

            return MessageResult.Success;
        }

        private MessageResult HandshakeSetup(Message msg)
        {
            if (this.State != MessageProtocolState.WaitSetup)
                return MessageResult.Error;

            this.KeyExchange.ReadSetup(msg);

            using (var msgChallenge = MessagePool.Rent())
            {
                msgChallenge.ID = ReqID;

                this.KeyExchange.WriteSetup(msgChallenge);
                this.KeyExchange.WriteChallenge(msgChallenge);

                _session.Send(msgChallenge);

                this.State = MessageProtocolState.WaitChallenge;
            }

            return MessageResult.Success;
        }
    }
}