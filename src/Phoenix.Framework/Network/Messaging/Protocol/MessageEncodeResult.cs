﻿namespace Phoenix.Framework.Network.Messaging.Protocol
{
    internal enum ProtocolEncodeResult
    {
        Success,
        InvalidMsgSize,
    }
}