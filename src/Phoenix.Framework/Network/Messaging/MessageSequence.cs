﻿namespace Phoenix.Framework.Network.Messaging
{
    internal sealed class MessageSequence
    {
        private const uint DEFAULT_SEED = 0x9ABFB3B6;
        private byte _byte0;
        private readonly byte _byte1;
        private readonly byte _byte2;

        public MessageSequence(uint seed)
        {
            var mut0 = seed != 0 ? seed : DEFAULT_SEED;
            var mut1 = GenerateValue(ref mut0);
            var mut2 = GenerateValue(ref mut0);
            var mut3 = GenerateValue(ref mut0);
            var mut4 = GenerateValue(ref mut0);

            _byte1 = (byte)((mut1 & byte.MaxValue) ^ (mut2 & byte.MaxValue));
            if (_byte1 == 0)
                _byte1 = 1;

            _byte2 = (byte)((mut0 & byte.MaxValue) ^ (mut3 & byte.MaxValue));
            if (_byte2 == 0)
                _byte2 = 1;

            _byte0 = (byte)(_byte2 ^ _byte1);
        }

        private static uint GenerateValue(ref uint value)
        {
            for (int i = 0; i < 32; i++)
            {
                var v = value;
                v = (v >> 2) ^ value;
                v = (v >> 2) ^ value;
                v = (v >> 1) ^ value;
                v = (v >> 1) ^ value;
                v = (v >> 1) ^ value;
                value = (((value >> 1) | (value << 31)) & ~1u) | (v & 1);
            }

            return value;
        }

        public byte Next()
        {
            var value = (byte)(_byte2 * (~_byte0 + _byte1));
            return _byte0 = (byte)(value ^ (value >> 4));
        }
    }
}