﻿namespace Phoenix.Framework.Network.Messaging
{
    public enum MessageResult : byte
    {
        Unhandled = 0,
        Success = 1,
        Error = 2,
    }
}