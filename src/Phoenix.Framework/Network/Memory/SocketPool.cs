﻿using Phoenix.Framework.Memory;

using System.Net.Sockets;

namespace Phoenix.Framework.Network.Memory
{
    public class SocketPool : ObjectPool<Socket>, ISocketPool
    {
        public SocketPool(int size) : base(size, true)
        {
        }

        protected override Socket Create()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            {
                Blocking = false,

                // Disable the Nagle Algorithm for this tcp socket.
                NoDelay = true,

                // Linger for 0 seconds after Socket.Close(), to prevent TIME_WAIT
                LingerState = new LingerOption(true, 0),

                ReceiveBufferSize = NetHelper.BUFFER_SIZE,
                SendBufferSize = NetHelper.BUFFER_SIZE,
            };
        }

        protected override void Destroy(in Socket obj)
        {
            obj.Close();
        }
    }
}