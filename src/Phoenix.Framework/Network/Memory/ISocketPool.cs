﻿using Phoenix.Framework.Memory;

using System.Net.Sockets;

namespace Phoenix.Framework.Network.Memory
{
    internal interface ISocketPool : IObjectPool<Socket>
    {

    }
}