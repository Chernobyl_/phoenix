﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Phoenix.Framework.Network
{
    internal static class NetHelper
    {
        internal const int BUFFER_SIZE = 4096;

        public static Socket CreateTcpSocket() => new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public static Socket CreateUdpSocket() => new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        public static IPEndPoint ToEndPoint(string hostOrIP, ushort port)
        {
            if (!IPAddress.TryParse(hostOrIP, out IPAddress address))
                address = Array.Find(Dns.GetHostEntry(hostOrIP).AddressList, p => p.AddressFamily == AddressFamily.InterNetwork);

            return new IPEndPoint(address, port);
        }

        public static string AddressFromEndPoint(EndPoint endPoint) => (endPoint as IPEndPoint)?.ToString();

        public static IPAddress IPAddressFromEndPoint(EndPoint ep) => (ep as IPEndPoint)?.Address;

        public static int PortFromEndPoint(EndPoint ep) => (ep as IPEndPoint)?.Port ?? 0;

        public static IPAddress IdentifyIPAddress()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
                return IPAddress.Any;

            var hostName = Dns.GetHostName();
            var hostEntry = Dns.GetHostEntry(hostName);
            return Array.Find(hostEntry.AddressList, p => p.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}