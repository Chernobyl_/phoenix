﻿using NLog;

using Phoenix.Framework.Network;
using Phoenix.Framework.Network.Config;
using Phoenix.Framework.Network.Messaging;

using System;

namespace SampleClient
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "SampleClient";
            var service = new SampleClientService();
            service.Run();
            Console.ReadLine();
        }
    }

    internal class SampleClientService
    {
        public SampleClientNet Network { get; set; }

        public string Name { get; set; }

        public SampleClientService()
        {
            this.Name = "SampleClientService";
            this.Network = new SampleClientNet(this, new SampleClientNetworkConfig());
        }

        public void Run()
        {
            var session = this.Network.Connect("127.0.0.1", 16000);
            session.Ready += this.Session_Ready;
        }

        private void Session_Ready(NetSession session)
        {
            this.TestSend(session);
        }

        internal void TestSend(NetSession session)
        {
            using (var msgAck = MessagePool.Rent())
            {
                msgAck.ID = (MessageID)0x2001;
                msgAck.TryWrite(this.Name);
                msgAck.TryWrite(byte.MinValue);
                msgAck.Size = 4090;

                session.Send(msgAck);
            }
        }
    }

    internal class SampleClientNetworkConfig : INetClientConfig
    {
        public bool KeepAlive => true;
        public int ConnectionTimeout => 5000;

        public int SocketCount => 1024;

        public int TaskCount => 1;

        public INetSessionFactory NetSessionFactory => SampleClientNetSession.Factory;
    }

    internal class SampleClientNetSession : NetSessionClient
    {
        #region Factory

        private class SampleClientNetSessionFactory : INetSessionFactory
        {
            public NetSession Create(NetEngine engine, int id) => new SampleClientNetSession(engine, id);
        }

        public static INetSessionFactory Factory { get; } = new SampleClientNetSessionFactory();

        #endregion Factory

        public SampleClientNetSession(NetEngine engine, int id) : base(engine, id)
        {
        }
    }

    internal class SampleClientNet : NetEngineClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly SampleClientService _service;

        private readonly SampleClientHandler _sampleHandler;

        public SampleClientNet(SampleClientService service, INetClientConfig config) : base(config)
        {
            _service = service;

            _sampleHandler = new SampleClientHandler(_service, this.MessageManager);
        }

        protected override void OnConnected(NetSession session)
        {
            base.OnConnected(session);
            Logger.Info($"Connected: {session.ID}");
        }

        protected override void OnDisconnected(NetSession session, NetDisconnectReason reason)
        {
            base.OnDisconnected(session, reason);
            Logger.Info($"Disconnected: {session.ID}: {reason}");
        }
    }

    internal class SampleClientHandler : MessageHandler
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly SampleClientService _service;

        public SampleClientHandler(SampleClientService service, MessageManager manager) : base(manager)
        {
            _service = service;

            // Register handlers
            manager.RegisterHandler(MessageID.Create(MessageDirection.NoDir, MessageType.Framework, 0x0001), this.SampleHandler1);
        }

        private MessageResult SampleHandler1(NetSession session, Message msg)
        {
            if (!msg.TryRead(out string remoteService))
                return MessageResult.Error;

            //Logger.Info(remoteService);

            _service.TestSend(session);

            return MessageResult.Success;
        }
    }
}