﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Phoenix.Framework.Database.Migrations
{
    public partial class InitialRelease : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Account");

            migrationBuilder.EnsureSchema(
                name: "Logs");

            migrationBuilder.EnsureSchema(
                name: "System");

            migrationBuilder.CreateTable(
                name: "Announcements",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 80, nullable: true),
                    Message = table.Column<string>(maxLength: 1024, nullable: true),
                    EditDatetime = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Announcements", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SecurityGroups",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    IsDisabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecurityGroups", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SecurityPolicies",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    IsDisabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecurityPolicies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ModuleVersions",
                schema: "System",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Version = table.Column<int>(nullable: false),
                    Module = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsValid = table.Column<bool>(nullable: false),
                    RecordUpdateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleVersions", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    RegIP = table.Column<string>(nullable: true),
                    RegDateTime = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    SecurityGroupID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_SecurityGroups_SecurityGroupID",
                        column: x => x.SecurityGroupID,
                        principalSchema: "Account",
                        principalTable: "SecurityGroups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SecurityGroupPolicies",
                schema: "Account",
                columns: table => new
                {
                    SecurityGroupID = table.Column<int>(nullable: false),
                    SecurityPolicyID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecurityGroupPolicies", x => new { x.SecurityGroupID, x.SecurityPolicyID });
                    table.ForeignKey(
                        name: "FK_SecurityGroupPolicies_SecurityGroups_SecurityGroupID",
                        column: x => x.SecurityGroupID,
                        principalSchema: "Account",
                        principalTable: "SecurityGroups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SecurityGroupPolicies_SecurityPolicies_SecurityPolicyID",
                        column: x => x.SecurityPolicyID,
                        principalSchema: "Account",
                        principalTable: "SecurityPolicies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ModuleVersionFiles",
                schema: "System",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModuleVersionID = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    FileDest = table.Column<string>(nullable: true),
                    FileSize = table.Column<int>(nullable: false),
                    FileType = table.Column<byte>(nullable: false),
                    ToBePacked = table.Column<bool>(nullable: false),
                    IsValid = table.Column<bool>(nullable: false),
                    RecordUpdateStamp = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleVersionFiles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ModuleVersionFiles_ModuleVersions_ModuleVersionID",
                        column: x => x.ModuleVersionID,
                        principalSchema: "System",
                        principalTable: "ModuleVersions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PrivilegedIPs",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IPBegin = table.Column<string>(nullable: false),
                    IPEnd = table.Column<string>(nullable: false),
                    GMAccountID = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IssueDateTime = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ExpirationDateTime = table.Column<DateTime>(nullable: false, defaultValueSql: "DATEADD(MONTH, 6, GETDATE())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivilegedIPs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PrivilegedIPs_Users_GMAccountID",
                        column: x => x.GMAccountID,
                        principalSchema: "Account",
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Punishments",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExecutorID = table.Column<int>(nullable: true),
                    UserID = table.Column<int>(nullable: true),
                    Reason = table.Column<string>(maxLength: 1024, nullable: true),
                    Type = table.Column<decimal>(nullable: false),
                    TimeBegin = table.Column<DateTime>(nullable: false),
                    TimeEnd = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Punishments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Punishments_Users_ExecutorID",
                        column: x => x.ExecutorID,
                        principalSchema: "Account",
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Punishments_Users_UserID",
                        column: x => x.UserID,
                        principalSchema: "Account",
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Silks",
                schema: "Account",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: true),
                    Own = table.Column<int>(nullable: false),
                    Gift = table.Column<int>(nullable: false),
                    Point = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Silks", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Silks_Users_UserID",
                        column: x => x.UserID,
                        principalSchema: "Account",
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LoginHistory",
                schema: "Logs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    IP = table.Column<string>(nullable: true),
                    HWID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_LoginHistory_Users_UserID",
                        column: x => x.UserID,
                        principalSchema: "Account",
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrivilegedIPs_GMAccountID",
                schema: "Account",
                table: "PrivilegedIPs",
                column: "GMAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Punishments_ExecutorID",
                schema: "Account",
                table: "Punishments",
                column: "ExecutorID");

            migrationBuilder.CreateIndex(
                name: "IX_Punishments_UserID",
                schema: "Account",
                table: "Punishments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_SecurityGroupPolicies_SecurityPolicyID",
                schema: "Account",
                table: "SecurityGroupPolicies",
                column: "SecurityPolicyID");

            migrationBuilder.CreateIndex(
                name: "IX_Silks_UserID",
                schema: "Account",
                table: "Silks",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SecurityGroupID",
                schema: "Account",
                table: "Users",
                column: "SecurityGroupID");

            migrationBuilder.CreateIndex(
                name: "IX_LoginHistory_UserID",
                schema: "Logs",
                table: "LoginHistory",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVersionFiles_ModuleVersionID",
                schema: "System",
                table: "ModuleVersionFiles",
                column: "ModuleVersionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Announcements",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "PrivilegedIPs",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "Punishments",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "SecurityGroupPolicies",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "Silks",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "LoginHistory",
                schema: "Logs");

            migrationBuilder.DropTable(
                name: "ModuleVersionFiles",
                schema: "System");

            migrationBuilder.DropTable(
                name: "SecurityPolicies",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Account");

            migrationBuilder.DropTable(
                name: "ModuleVersions",
                schema: "System");

            migrationBuilder.DropTable(
                name: "SecurityGroups",
                schema: "Account");
        }
    }
}
