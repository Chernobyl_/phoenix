﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class Silk
    {
        public int ID { get; set; }
        public User User { get; set; }
        public int Own { get; set; }
        public int Gift { get; set; }
        public int Point { get; set; }

    }
}
