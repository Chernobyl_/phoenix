﻿using System.Collections.Generic;

namespace Phoenix.Framework.Database.Models.Account
{
    public class SecurityPolicy : Security
    {
        public IEnumerable<SecurityGroupPolicies> SecurityGroupPolicies { get; set; }
    }
}