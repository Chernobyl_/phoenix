﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class SecurityGroupPolicies
    {
        public int SecurityGroupID { get; set; }
        public SecurityGroup SecurityGroup { get; set; }

        public int SecurityPolicyID { get; set; }
        public SecurityPolicy SecurityPolicy { get; set; }
    }
}
