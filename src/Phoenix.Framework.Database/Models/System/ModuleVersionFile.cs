﻿using System;
using System.Collections.Generic;

namespace Phoenix.Framework.Database.Models.System
{
    public class ModuleVersionFile
    {
        public int ID { get; set; }
        public ModuleVersion ModuleVersion { get; set; }
        public string FileName { get; set; }
        public string FileDest { get; set; }
        public int FileSize { get; set; }
        public FileType FileType { get; set; }
        public bool ToBePacked { get; set; }
        public bool IsValid { get; set; } = true;
        public DateTime RecordUpdateStamp { get; set; }
    }
}