﻿namespace Phoenix.Framework.Database.Models.System
{
    public enum Module
    {
        GlobalManager,
        MachineManager,
        GatewayServer,
        DownloadServer,
        AreaServer,
        WorldServer
    }
}