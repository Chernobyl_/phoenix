﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Framework.Database.Persistence.Repositories
{
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Add object to <see cref="TEntity"/> collection 
        /// </summary>
        /// <param name="entity">Single <see cref="TEntity"/> object</param>
        void Add(TEntity entity);

        /// <summary>
        /// Remove <see cref="TEntity"/> element from the collection
        /// </summary>
        /// <param name="entity">Single <see cref="TEntity"/> object</param>
        void Remove(TEntity entity);

        /// <summary>
        /// Get all <see cref="TEntity"/> elements
        /// </summary>
        /// <returns><see cref="TEntity"/> collection</returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Get all async <see cref="TEntity"/> elements
        /// </summary>
        /// <returns><see cref="TEntity"/> collection</returns>
        Task<IEnumerable<TEntity>> GetAllAsync();

        /// <summary>
        /// Get single <see cref="TEntity"/> element by Id
        /// </summary>
        /// <param name="id"><see cref="TEntity"/> Identifier</param>
        /// <returns>Single <see cref="TEntity"/> object</returns>
        TEntity Get<TVal>(TVal id) where TVal : struct;

        /// <summary>
        /// Get single Async <see cref="TEntity"/> element by Id
        /// </summary>
        /// <param name="id"><see cref="TEntity"/> Identifier</param>
        /// <returns>Single <see cref="TEntity"/> object</returns>
        Task<TEntity> GetAsync<TVal>(TVal id) where TVal : struct;

        /// <summary>
        /// Get the objects found in the <see cref="TEntity"/> collection
        /// </summary>
        /// <param name="predicate">Search criterias</param>
        /// <returns>Found elements</returns>
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Get Many Async <see cref="TEntity"/> element by Id
        /// </summary>
        /// <param name="id"><see cref="TEntity"/> Identifier</param>
        /// <returns>Single <see cref="TEntity"/> object</returns>
        Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Get the objects found in the <see cref="TEntity"/> collection
        /// </summary>
        /// <param name="predicate">Search criterias</param>
        /// <returns>Found elements</returns>
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Get the first object found in the <see cref="TEntity"/> collection
        /// </summary>
        /// <param name="predicate">Search criterias</param>
        /// <returns>Found elements</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);


        bool Exists<TVal>(TVal id) where TVal : struct;
        bool Exists(Expression<Func<TEntity, bool>> predicate);
    }
}
